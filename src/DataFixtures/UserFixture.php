<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\User;

class UserFixture extends Fixture
{
    public function load(ObjectManager $manager)
    {

        $user = new User();
        $user->setEmail('admin@gmail.com');
        $user->setPassword('123456');
        $manager->persist($user);
        // $product = new Product();
        // $manager->persist($product);


        $manager->flush();
    }
}
