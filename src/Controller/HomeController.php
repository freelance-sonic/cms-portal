<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\UserRole;
use App\Message\Services\EmailServiceInterface;
use App\Message\Services\MessageInterface;
use App\Message\Services\MessageService;
use App\Message\Services\SendMessageChainInterface;
use App\Security\EmailVerifier;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

class HomeController extends AbstractController
{
    /** @var SendMessageChainInterface */
    private SendMessageChainInterface $sendMessageChain;
    private MessageInterface $messageService;

    public function __construct(SendMessageChainInterface $sendMessageChain, MessageInterface $messageService)
    {
        $this->sendMessageChain = $sendMessageChain;
        $this->messageService = $messageService;
    }

    /**
     * @Route("/react/{reactRouting}", name="home", defaults={"reactRouting": null})
     * @param UserInterface $user
     * @return Response
     */
    public function index(UserInterface $user)
    {

        dd($this->messageService);
        //dd($this->sendMessageChain->getTransports());
        //testing lazy load via proxies
//
//        if ($user instanceof User) {
//            $user->getUserRoles()->getValues();
//           // dd(get_class($user));
//        }
        //$users = $this->getDoctrine()->getRepository(User::class)->find(46);
//        $role =  $this->getDoctrine()->getRepository(UserRole::class)->find(2);
////
//        if (!$role instanceof UserRole)
//            $role->getUser()->getEmail();
//
//        dd($role);

        //dd(get_class($users));




        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',
        ]);
    }

    /**
     * @Route("/api/users", name="users")
     * @return Response
     */
    public function getUsers()
    {
        $users = [
            [
                'id' => 1,
                'name' => 'Olususi Oluyemi',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation',
                'imageURL' => 'https://randomuser.me/api/portraits/women/50.jpg'
            ],
            [
                'id' => 2,
                'name' => 'Camila Terry',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation',
                'imageURL' => 'https://randomuser.me/api/portraits/men/42.jpg'
            ],
            [
                'id' => 3,
                'name' => 'Joel Williamson',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation',
                'imageURL' => 'https://randomuser.me/api/portraits/women/67.jpg'
            ],
            [
                'id' => 4,
                'name' => 'Deann Payne',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation',
                'imageURL' => 'https://randomuser.me/api/portraits/women/50.jpg'
            ],
            [
                'id' => 5,
                'name' => 'Donald Perkins',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation',
                'imageURL' => 'https://randomuser.me/api/portraits/men/89.jpg'
            ]
        ];

        $response = new Response();

        $response->headers->set('Content-Type', 'application/json');
        $response->headers->set('Access-Control-Allow-Origin', '*');

        $response->setContent(json_encode($users));

        return $response;
    }


}
