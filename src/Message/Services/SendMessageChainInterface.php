<?php


namespace App\Message\Services;


interface SendMessageChainInterface
{
    public function getTransports() : array;
}