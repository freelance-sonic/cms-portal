<?php


namespace App\Message\Services;

/*
 * Auto services have tag 'app.message_tag' will be injected to this service
 * Inject services anywhere will already transports -> funny it can be include it's self but child will not have any transport
 */
class SendMessageChain implements SendMessageChainInterface
{
    const MAIL = 1;
    const PUSH_NOTIFICATION = 2;
    const SYSTEM = 3;

    /** @var MessageTransportInterface[]  */
    private array $transports;

    public function __construct()
    {
        $this->transports = [];
    }

    public function addTransport(MessageTransportInterface $transport)
    {
        $this->transports[] = $transport;
    }

    public function getTransports() : array
    {
        return $this->transports;
    }
}