<?php


namespace App\Message\Services;


interface MessageInterface
{
    public function getSendMessageChain() : SendMessageChainInterface;
}