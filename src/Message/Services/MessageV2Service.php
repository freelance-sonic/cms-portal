<?php


namespace App\Message\Services;


class MessageV2Service implements MessageInterface
{
    private SendMessageChainInterface $sendMessageChain;
    private string $emailAdmin;

    public function __construct(SendMessageChainInterface $sendMessageChain, $emailAdmin)
    {
        $this->sendMessageChain = $sendMessageChain;
        $this->emailAdmin = $emailAdmin;
    }

    public function getSendMessageChain() : SendMessageChainInterface {
        return $this->sendMessageChain;
    }
}