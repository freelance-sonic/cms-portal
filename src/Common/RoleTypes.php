<?php
/**
 * Created by PhpStorm.
 * User: jbb
 * Date: 12/6/2018
 * Time: 8:37 AM
 */

namespace App\Common;


interface RoleTypes
{
    const SYSADMIN = 'S';
    const ADMIN = 'A';
    const USER = 'U';

    const ALL = [
        self::SYSADMIN,
        self::ADMIN,
        self::USER,
    ];
}