<?php

namespace App\DependencyInjection\Compilers;


use App\Message\Services\SendMessageChain;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class MessageChainPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        // always first check if the primary service is defined
        if (!$container->has(SendMessageChain::class)) {
            return;
        }

        $definition = $container->findDefinition(SendMessageChain::class);

        // find all service IDs with the app.message_tag tag
        $taggedServices = $container->findTaggedServiceIds('app.message_tag');
        //dd($taggedServices);

        foreach ($taggedServices as $id => $tags) {
            // add the transport service to the SendMessageChain service
            $definition->addMethodCall('addTransport', [new Reference($id)]);
        }
    }
}