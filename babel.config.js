module.exports = function (api) {
    api.cache(true);
    return {
        "presets": [
            [
                "@babel/preset-env",
                {
                    "useBuiltIns": "entry",
                    "corejs": 3,
                    "targets": {
                        "ie": 11,
                        "chrome": 76,
                        "firefox": 72,
                        "ios": 13
                    },
                    "modules": false
                }
            ]
        ],
        "plugins": [
            [
                "@babel/plugin-proposal-decorators",
                {
                    "legacy": true
                }
            ],
            "@babel/transform-runtime",
            "@babel/plugin-transform-react-jsx",
            "@babel/plugin-syntax-dynamic-import",
            "@babel/plugin-transform-flow-strip-types",
            "@babel/plugin-proposal-class-properties",
        ],
        "comments": false,
        "env": {
            "test": {
                "plugins": [
                    "dynamic-import-node",
                    "@babel/plugin-transform-modules-commonjs"
                ]
            }
        }
    };
}