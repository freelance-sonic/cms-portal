const {merge} = require('webpack-merge');
const common = require('./webpack.config.js');
const webpack = require('webpack');
const path = require('path');
const TerserPlugin = require('terser-webpack-plugin');

process.env.APP_VERSION = Math.round((new Date()).getTime() / 1000).toString();

module.exports = merge(common, {
    mode: 'production',
    devtool: 'source-map',
    output: {
        filename: '[name].[contenthash].bundle.js',
        chunkFilename: '[contenthash].bundle.js',
    },
    optimization: {
        minimizer: [new TerserPlugin({
            cache: true,
            parallel: true,
            terserOptions: {
                compress: false,
                ecma: 5,
                mangle: true,
                keep_classnames: true,
                keep_fnames: true
            },
            sourceMap: true
        })],
        splitChunks: {
            minSize: 30000,
            // maxSize: 50000,
            maxAsyncRequests: 15,
            maxInitialRequests: 15,
        }
    }
});