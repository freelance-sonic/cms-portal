const path = require('path');
const webpackCommon = require('webpack');

module.exports = {
    entry: {
        'main': path.resolve(__dirname, 'assets/js/app.js'),
    },
    output: {
        filename: '[name].bundle.js',
        path: path.resolve(__dirname, 'public/dist'),
        publicPath: '/dist/'
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx|mjs|tsx|ts)$/,
                include: [
                    path.resolve(__dirname, 'node_modules', '@apollo'),
                    path.resolve(__dirname, 'node_modules', 'swiper'),
                    path.resolve(__dirname, 'node_modules', 'dom7'),
                ],
                use: [
                    {
                        loader: 'babel-loader',
                        options: {
                            presets: ['@babel/preset-env']
                        }
                    }
                ]
            },
            {
                test: /\.(tsx|ts)$/,
                include: [
                    path.resolve(__dirname, 'assets'),
                ],
                use: [
                    {
                        loader: "ts-loader",
                        query: {
                            'compilerOptions': {
                                "noEmit": false, // make sure js files do get emitted
                                "module": "esnext",
                            }
                        },
                    },
                    'eslint-loader',
                ]
            },
            {
                test: /\.(js|jsx|mjs)$/,
                include: [
                    path.resolve(__dirname, 'assets'),
                ],
                use: [
                    // 'eslint-loader',
                    {
                        loader: 'babel-loader',
                        query: {
                            cacheDirectory: true
                        }
                    }
                ],
            },
            {
                test: /(.scss|.css)$/,
                use: [
                    "style-loader", // creates style nodes from JS strings
                    "css-loader", // translates CSS into CommonJS
                    "sass-loader" // compiles Sass to CSS
                ]
            }
        ]
    },
    plugins: [
        new webpackCommon.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV || 'production'),
            'process.env.APP_VERSION': JSON.stringify(process.env.APP_VERSION || 'dev'),
        })
    ],
    resolve: {
        modules: [
            //path.resolve("./assets"),
            'node_modules'
        ],
        extensions: ['.jsx','.js', '.json', '.ts', '.tsx']
    }
};